Link to repo: https://bitbucket.org/rainbots/reflow-oven/src/master/

Clone this repo:

    git clone https://rainbots@bitbucket.org/rainbots/reflow-oven.git

# Table of Contents
- [Reflow oven settings and procedure](#markdown-header-reflow-oven-settings-and-procedure)

---e-n-d---

# Reflow advice from AMI

Advice from *AMI* on *temperature* and *rate of temperature change*:

- reflow is *liquidus* at 220°C
    - if temperature is too low, reflow never happens
- *flux is lost* as paste is heated
    - ramp temperature *too fast* and *flux is lost* before reflow starts
    - *flux is required* during reflow

# Reflow oven settings and procedure

On Fri, Feb 27, 2015 at 6:46 PM, Mike Gazes:

> This is the 4-step oven procedure I ended up following. It requires a stop watch (the one on my Android phone worked great).
> 
> Using this sequence, Andrei had a good result (all pads covered, no bridged joints) on the 0.4mm pitch. Of course, that had much more to do with his paste dispensing technique than it did with the oven timing.

1. Setup:
    - Set *first knob* to **Toast**.
        - *Toast* is the **maximum temperature** on toaster ovens.
    - Set *second knob* to **Bake**.
    - Set timer to ~20 minutes.
        - 20 minutes is a safety precaution to make sure the oven eventually turns off completely.

2. Preheat the oven:
    - Set the *Delta Temperature Controller* to 140°C.
    - When you hear the relay *click*, the oven is **finished pre-heating**.

3. PCB Soak:
    - Insert your board.
    - *Wait* one minute.
    - Set the *Delta Temperature Controller* to 180°C.
    - Change from *Bake* to *Toast*.
    - It takes exactly one minute for the temperature to rise from 150°C to 180°C.

4. Reflow Zone:
    - When the oven reaches 220°C, switch from *Toast* to *Bake*.
    - When the oven reaches 230°C, wait 10 seconds, then pull open the oven door and pull the tray out so the board can start to cool.
    - Reduce the Delta Controller temperature back to 140°C.
    - When the thermocouple reads 150°C, it is safe to extract the board.
    - Close the lid so the oven can go back to preheat mode.

Here are the actual stop-watch-measured times from our most recent run:

- Preheat with board (140°C): 1m 36s
- PCB Soak (150°C to 180°C): 55s
- Reflow Zone (180°C to 230°C): 56s
- Sit at peak (230°C): 18s

# Reflow attempts in an email from 2015-11-18
On Wed, Nov 18, 2015 at 5:58 PM, Mike Gazes:

> - Andrei tested reflow with IR umbrella
> - oven set to "Toast" (not "Bake")
> - temperature ramp unaffected by IR umbrella (happily surprised)
> - Andrei reflowed two detectors:
> - looks successful
> - test for detector damage pending on mating motherboard
> - next step: try oven on "Bake" (for convection)
> - reflow batch of 5 is on hold until we have FFC- connector daughterboards
> - Andrei's profile on "Toast" (time is relative to end of previous step):
> - soak 2min at 150degC
> - set to 180degC
> - reach 180degC in 2min
> - set to 240degC
> - reach 240degC in 3min, only above 220degC for ~20s
> - open oven


